


const listColor = ["pallet","viridian","pewter","cerulean","vermillion","lavender","celadon","saffron","fuschia","cinnabar"];
const getElement = id => document.getElementById(id);

btnColor = () => {
    let contentHTML = "";
    listColor.map(color => {
        color === 'pallet' ? contentHTML += `<button id='${color}' class='color-button ${color} active' onclick='addClassActive("${color}")'></button>`
        : contentHTML += `<button id='${color}' class='color-button ${color}' onclick='addClassActive("${color}")'></button>`;
    });
    getElement("colorContainer").innerHTML = contentHTML;
};

addClassActive = (color) => {
    let btns = document.getElementsByClassName("color-button");
    let house = getElement("house");

    for( let btn of btns) {
        if(btn.classList.contains('active')) {
            btn.classList.remove('active')
            house.classList.remove(btn.getAttribute('id'));
        }
    }
    document.getElementsByClassName(`color-button ${color}`)[0].classList.add('active');
    house.classList.add(color);
};

btnColor();

