const getEle = id => document.getElementById(id);

tinhToan = (id) => {
    if (id === 'tbKhoi1') {
        let a = getEle('inpToan').value,
            b = getEle('inpLy').value,
            c = getEle('inpHoa').value
        tinhDiemTrungBinh(id, a, b, c);
    } else {
        let d = getEle('inpVan').value,
            e = getEle('inpSu').value,
            f = getEle('inpDia').value,
            g = getEle('inpEnglish').value
        tinhDiemTrungBinh(id, d, e, f, g);
    }
}

tinhDiemTrungBinh = (id, ...rest) => {
    let tong = 0;
    rest.map(num => {
        tong += num * 1;
    })
    getEle(id).innerHTML = tong / rest.length;
}


